import { createRouter, createWebHashHistory } from 'vue-router'

const routes = [
  {
    path: '/',
    component: () => import('@/views/home/Home.vue'),
    name: '首页'
  },
  {
    path: '/tags',
    component: () => import('@/views/tag/Tag.vue'),
    name: '标签'
  },
  {
    path: '/categories',
    component: () => import('@/views/category/Category.vue'),
    name: '分类'
  },
  {
    path: '/archives',
    component: () => import('@/views/archive/Archive.vue'),
    name: '归档'
  },
  {
    path: '/about',
    component: () => import('@/views/about/About.vue'),
    name: '关于我'
  },
  {
    path: '/articles/:articleId',
    component: () => import('@/views/article/Article.vue')
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

router.afterEach(() => {
  window.scrollTo(0, 0) // 切换路由之后滚动条始终在最顶部
})

export default router
