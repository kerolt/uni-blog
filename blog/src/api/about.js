import request from '../utils/request.js'

export const getAboutContent = () => {
  return request({
    url: '/about'
  })
}
