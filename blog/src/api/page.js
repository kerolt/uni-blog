import request from '@/utils/request.js'

export const getPageByName = (pageName) => {
  return request({
    url: `/pages/${pageName}`
  })
}
