import express from 'express'
import Page from '../models/page.js'
import result from '../utils/result.js'

const pageRouter = express.Router()

pageRouter.get('/', async (req, res) => {
  if ((await Page.find()).length === 0) {
    new Page({ pageName: '主页', bannerUrl: 'http://cdn.liuyx.cc/wallhaven-m3dm1k.png' }).save()
    new Page({
      pageName: '分类',
      bannerUrl: 'https://typora-lyx.oss-cn-guangzhou.aliyuncs.com/typora/wallhaven-kxw9o1.png'
    }).save()
    new Page({
      pageName: '标签',
      bannerUrl: 'https://typora-lyx.oss-cn-guangzhou.aliyuncs.com/typora/wallhaven-kxw3p1.jpg'
    }).save()
    new Page({
      pageName: '归档',
      bannerUrl: 'https://typora-lyx.oss-cn-guangzhou.aliyuncs.com/typora/wallhaven-1p38v3.png'
    }).save()
    new Page({
      pageName: '关于我',
      bannerUrl: 'https://typora-lyx.oss-cn-guangzhou.aliyuncs.com/typora/wallhaven-p931d9.png'
    }).save()
  }
  const pageList = await Page.find({})
  res.status(200).json(result(200, '页面图片查询成功', pageList))
})

pageRouter.get('/:pageName', async (req, res) => {
  const pageName = req.params.pageName
  const page = await Page.findOne({ pageName })
  res.status(200).json(result(200, '获取页面图片成功', page))
})

pageRouter.put('/', async (req, res) => {
  const { _id, bannerUrl } = req.body
  await Page.updateOne({ _id }, { bannerUrl })
  res.status(200).json(result(200, '修改页面图片成功', null))
})

export default pageRouter
