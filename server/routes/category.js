import express from 'express'
import Category from '../models/category.js'
import result from '../utils/result.js'

const categpryRouter = express.Router()

// 获取所有标签
categpryRouter.get('/', (req, res) => {
  Category.find({}, (err, doc) => {
    if (err) {
      res.status(500).json(result(500, '查询出错啦', null))
    }
    res.status(200).json(result(200, '查询成功', doc))
  })
})

// 根据id获取标签
categpryRouter.get('/:id', async (req, res) => {
  const category = await Category.findOne({ _id: req.params.id })
  res.status(200).json(result(200, null, category))
})

// 根据id是否为空字符串来实现新增或修改
categpryRouter.post('/', async (req, res) => {
  const data = req.body
  if (data._id === '') {
    await Category.create({ name: data.name })
    res.status(200).json(result(200, '新增成功', null))
  } else {
    await Category.updateOne({ _id: data._id }, { name: data.name })
    res.status(200).json(result(200, '修改成功', null))
  }
})

// 根据id删除标签
categpryRouter.delete('/:id', async (req, res) => {
  await Category.deleteOne({ _id: req.params.id })
  res.status(200).json(result(200, '删除成功', null))
})

export default categpryRouter
