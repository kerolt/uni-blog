/*
 * @Author: liuyx 1517482303@qq.com
 * @Date: 2022-11-23 10:44:22
 * @LastEditTime: 2022-12-17 09:22:16
 * @Description: 注册路由
 */
import token from './token.js'
import article from './article.js'
import tag from './tag.js'
import category from './category.js'
import site from './site.js'
import about from './about.js'
import page from './page.js'

const routes = (app) => {
  app.use('/token', token)
  app.use('/articles', article)
  app.use('/tags', tag)
  app.use('/categories', category)
  app.use('/sites', site)
  app.use('/about', about)
  app.use('/pages', page)
}

export default routes
