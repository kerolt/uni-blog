import express from 'express'
import About from '../models/about.js'
import result from '../utils/result.js'

const aboutRouter = express.Router()

aboutRouter.get('/', async (req, res) => {
  const about = await About.findOne()
  console.log(about)
  if (about === null) {
    res.status(200).json(result(400, '暂时没有数据哦', { _id: '', content: '' }))
  } else {
    res.status(200).json(result(200, '查询成功', about))
  }
})

aboutRouter.put('/', async (req, res) => {
  const data = req.body
  if (data._id === '') {
    const about = new About({
      content: data.content
    })
    await about.save()
    res.status(200).json(result(200, '创建成功', null))
  } else {
    await About.updateOne({ _id: data._id }, data)
    res.status(200).json(result(200, '更新成功', null))
  }
  // await About.updateOne()
  console.log(data)
})

export default aboutRouter
