import express from 'express'
// import Article from '../models/article.js'
import Tag from '../models/tag.js'
import result from '../utils/result.js'

const tagRouter = express.Router()

// 获取所有标签
tagRouter.get('/', (req, res) => {
  Tag.find({}, (err, doc) => {
    if (err) {
      res.status(500).json(result(500, '查询出错啦', null))
    }
    res.status(200).json(result(200, null, doc))
  })
})

// 根据id获取标签
tagRouter.get('/:id', async (req, res) => {
  const tag = await Tag.findOne({ _id: req.params.id })
  res.status(200).json(result(200, null, tag))
})

// 根据id是否为空字符串来实现新增或修改
tagRouter.post('/', async (req, res) => {
  const data = req.body
  if (data._id === '') {
    await Tag.create({ name: data.name })
    res.status(200).json(result(200, '新增成功', null))
  } else {
    // const temp = await Article.find({ tagList: { $elemMatch: { _id: data._id } } })
    // console.log(temp)
    await Tag.updateOne({ _id: data._id }, { name: data.name })
    res.status(200).json(result(200, '修改成功', null))
  }
})

// 根据id删除标签
tagRouter.delete('/:id', async (req, res) => {
  await Tag.deleteOne({ _id: req.params.id })
  res.status(200).json(result(200, '删除成功', null))
})

export default tagRouter
