/*
 * @Author: liuyx 1517482303@qq.com
 * @Date: 2022-12-17 09:14:28
 * @LastEditTime: 2022-12-17 15:10:09
 * @Description: 网站信息相关操作
 */
import express from 'express'
import Site from '../models/Site.js'
import result from '../utils/result.js'

const siteRouter = express.Router()

siteRouter.get('/', async (req, res) => {
  const siteInfo = await Site.findOne()
  if (siteInfo === null) {
    res.status(500).json(result(500, '未能找到有关信息', null))
  } else {
    res.status(200).json(result(200, '查询成功', siteInfo))
  }
})

siteRouter.put('/', async (req, res) => {
  const data = req.body
  await Site.updateOne({ _id: data._id }, data)
  res.status(200).json(result(200, '更新成功', null))
})

export default siteRouter
