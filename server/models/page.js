import mongoose from 'mongoose'

const PageSchema = new mongoose.Schema({
  pageName: String,
  bannerUrl: String
})

export default mongoose.model('Page', PageSchema, 'page')
