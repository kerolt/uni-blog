/*
 * @Author: liuyx 1517482303@qq.com
 * @Date: 2022-12-17 08:19:03
 * @LastEditTime: 2022-12-17 09:38:01
 * @Description: 网站信息Schema
 */
import mongoose from 'mongoose'

const SiteSchema = mongoose.Schema({
  avatar: String,
  siteName: String,
  author: String,
  intro: String,
  sentence: String,
  date: Date,
  notice: String,
  github: String,
  gitee: String,
  icp: String
})

export default mongoose.model('Site', SiteSchema, 'site')
