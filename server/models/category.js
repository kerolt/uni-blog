import mongoose from 'mongoose'

const CategorySchema = new mongoose.Schema({
  name: String,
  articleNum: {
    type: Number,
    default: 0
  }
})

export default mongoose.model('Category', CategorySchema, 'category')
