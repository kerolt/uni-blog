// const mongoose = ('mongoose')
import mongoose from 'mongoose'

const ArticleSchema = new mongoose.Schema({
  cover: String,
  title: String,
  content: String,
  category: String,
  tagList: Array,
  views: {
    type: Number,
    default: 0
  },
  createTime: String,
  updateTime: String,
  about: String
})

export default mongoose.model('Article', ArticleSchema, 'article')
