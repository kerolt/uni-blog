import mongoose from 'mongoose'

const TagSchema = new mongoose.Schema({
  name: String,
  articleNum: {
    type: Number,
    default: 0
  }
})

export default mongoose.model('Tag', TagSchema, 'tag')
