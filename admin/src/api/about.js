import request from '@/utils/request.js'

export const getAboutContent = () => {
  return request({
    url: '/about'
  })
}

export const updateAboutContent = (data) => {
  return request({
    url: '/about',
    method: 'put',
    data
  })
}
