import request from '@/utils/request.js'

// 获取所有文章
export const getArticleList = (currentPage, pageSize) => {
  return request({
    url: '/articles',
    method: 'get',
    params: {
      currentPage,
      pageSize
    }
  })
}

// 根据id获取文章
export const getArticleById = (id) => {
  return request({
    url: `/articles/${id}`
  })
}

// 发布文章
export const postArticle = (data) => {
  return request({
    url: '/articles',
    method: 'post',
    data
  })
}

// 根据id删除文章
export const deleteArticleById = (id) => {
  return request({
    url: `/articles/${id}`,
    method: 'delete'
  })
}

// 批量删除文章
export const deleteMutipleArticle = (data) => {
  return request({
    url: '/articles',
    method: 'delete',
    data
  })
}
