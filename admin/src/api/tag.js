import request from '@/utils/request.js'

// 获取标签列表
export const getTagList = () => {
  return request({
    url: '/tags'
  })
}

// 根据id获取标签
export const getTagById = (id) => {
  return request({
    url: `/tags/${id}`
  })
}

/**
 * 新增or修改标签
 * @param {*} data tag表单
 * @returns axios响应结果
 */
export const updateTag = (data) => {
  return request({
    url: '/tags',
    method: 'post',
    data
  })
}

// 根据id删除标签
export const deleteTagById = (id) => {
  return request({
    url: `/tags/${id}`,
    method: 'delete'
  })
}
