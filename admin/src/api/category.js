import request from '@/utils/request.js'

// 获取分类列表
export const getCategoryList = () => {
  return request({
    url: '/categories'
  })
}

// 根据id获取分类
export const getCategoryById = (id) => {
  return request({
    url: `/categories/${id}`
  })
}

/**
 * 新增or修改分类
 * @param {*} data categories表单
 * @returns axios响应结果
 */
export const updateCategory = (data) => {
  return request({
    url: '/categories',
    method: 'post',
    data
  })
}

// 根据id删除分类
export const deleteCategoryById = (id) => {
  return request({
    url: `/categories/${id}`,
    method: 'delete'
  })
}
