/*
 * @Author: liuyx 1517482303@qq.com
 * @Date: 2022-12-17 08:42:16
 * @LastEditTime: 2022-12-17 09:43:32
 * @Description: 网站信息API
 */
import request from '@/utils/request.js'

// 获取网站信息
export const getSiteInfo = () => {
  return request({
    url: '/sites'
  })
}

export const updateSiteInfo = (data) => {
  return request({
    url: '/sites',
    method: 'put',
    data
  })
}
